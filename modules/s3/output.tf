# data "aws_iam_policy_document" "website_policy" {
#   statement {
#     actions = [
#       "s3:GetObject"
#     ]
#     principals {
#       identifiers = ["*"]
#       type = "AWS"
#     }
#     resources = [
#       "arn:aws:s3:::${var.bucket_name}/*"
#     ]
#   }
# }
output "bucket_domain_name" {
  value = aws_s3_bucket.b.bucket_regional_domain_name
}
output "domain_names" {
    value = aws_s3_bucket.www_bucket.website_endpoint
}