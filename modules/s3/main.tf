resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name

  tags = {
    Name = var.bucket_tag
  }
 }

# resource "aws_s3_bucket_object" "object" {
#   bucket = var.bucket_name
#   key    = var.key
#   # source = "/home/yashvishah/Desktop/Terraform sample code/terraform-server-02/modules/ec2/ec2-script.sh"
# }

# resource "aws_s3_bucket_versioning" "versioning_example" {
#   bucket = var.bucket_name
#   versioning_configuration {
#     status = "Enabled"
#   }
# }
# resource "aws_s3_bucket" "website_bucket" {
#   bucket = var.bucket_name
#   acl = "public-read"
#   policy = data.aws_iam_policy_document.website_policy.json
#   website {
#     index_document = "ec2-scrit.sh"
#     error_document = "ec2-scrit.sh"
#   }
# }
resource "aws_s3_bucket" "www_bucket" {
  bucket = "www.${var.bucket_name}"
  acl = "public-read"
  # policy = templatefile("/home/yashvishah/Desktop/Terraform sample code/terraform-server-02/modules/s3/policy.json", { bucket = "www.${var.bucket_name}" })

  cors_rule {
    allowed_headers = ["Authorization", "Content-Length"]
    allowed_methods = ["GET", "POST"]
    allowed_origins = ["https://www.${var.domain_name}"]
    max_age_seconds = 3000
  }

  website {
    index_document = "index.html"
    error_document = "404.html"
  }

}

# S3 bucket for redirecting non-www to www.
# resource "aws_s3_bucket" "root_bucket" {
#   bucket = var.bucket_name
#   acl = "public-read"
#   policy = templatefile("policy.json", { bucket = var.bucket_name })

#   website {
#     redirect_all_requests_to = "https://www.${var.domain_name}"
#   }

#   tags = var.common_tags
# }
