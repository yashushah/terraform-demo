resource "aws_acm_certificate" "cert" {
  domain_name       = var.wild_domain_name
  subject_alternative_names = [
    var.w_domain_name
  ]
  validation_method = "DNS"

  tags = {
    Environment = var.cert_managertag
  }

  lifecycle {
    create_before_destroy = true
  }
}
