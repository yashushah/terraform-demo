variable "aws_access_key" {
  type =  string
  default = "AKIAQIHTTF2JDDJKW7UT"
}

variable "aws_secret_key" {
  type =  string
  default = "AB8y20S+g2RFnCsL/QD+mv7BtAHj2Q/9VEY9Ff0l"
}

variable "region" {
    type = string
    default = "ap-south-1"
}

variable "vpc_name" {
    type = string
    default = "demo-vpc-tf"
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
}

variable "public_subnet_az1_cidr" {
    type = string
    default = "10.0.1.0/24"
}


variable "public_subnet_az2_cidr" {
    type = string
    default = "10.0.2.0/24"
}

variable "private_subnet_az1_cidr" {
    type = string
    default = "10.0.10.0/24"
}


variable "private_subnet_az2_cidr" {
    type = string
    default = "10.0.11.0/24"
}

# Security Group for vpc
variable "security_group_name" {
  type = string
  default = "demo-sg-tf"
}

# EC2
variable "key_name" {
  type = string
  default = "yashu015" 
}

variable "ec2_ami" {
  type = string
  default = "ami-006d3995d3a6b963b"
}

variable "ec2_type" {
  type = string
  default = "t2.micro"
}

variable "ec2_name" {
  type = string
  default = "demo-ec2-tf"
}


# Create RDS

variable "allocated_storage" {
  type = string
  default = "20"
}

variable "db_engine" {
  type = string
  default = "mysql"
}

variable "engine_version" {
  type = string
  default = "8.0.28"
}

variable "instance_class" {
  type = string
  default = "db.t2.micro"
}

variable "db_name" {
  type = string
  default = "demo-rds-tf"
}

variable "username" {
  type = string
  default = "admin"
}

variable "password" {
  type = string
  default = "Admin@123"
}

variable "skip_final_snapshot" {
  type = bool
  default = "false"
}

variable "db_subnet_group_name" {
  type = string
  default = "rds_database_subnet"
}

variable "subnet_group_tag" {
  type = string
  default = "rds_databse_saabnet"
}

#S3 Bucket
variable "bucket_name" {  
  type        = string  
  default = "thought.tk"
} 

variable "bucket_tag" {  
  type        = string  
  default = "thought.tk"
} 

variable "key" {
    type = string
    default = "ec2-script.sh"
}

# Cert Manager
variable "wild_domain_name" {  
  type        = string  
  default = "*.thought.tk"
}
variable "w_domain_name" {  
  type        = string  
  default = "www.thought.tk"
}
variable "cert_managertag" {  
  type        = string  
  default = "thought.tk-certmanager"
}
# Route 53
variable "domain_name" {  
  type        = string  
  default = "thought.tk"
} 

